<?php
function dump($var)
{
    if(is_bool($var)||is_null($var)){
        var_dump($var);
    }else{
        echo "<pre style='white-space: pre-wrap;border: 1px solid #aaa;padding: 20px;font-size: 1.2em;background-color: #bbb;color: #000;line-height: 1.5em'>" . print_r($var,true) . "</pre>";
    }
}

function halt($var)
{
    dump($var);
    exit();
}

//获取当前IP地址
function getIP(){
    if(!empty($_SERVER['HTTP_CLIENT_IP'])){
        //ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        //ip pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

/**
 * 获取请求参数 $_REQUEST
 * @param string $name 参数键
 * @param string $default 不存在默认值
 * @return array|mixed|string 返回
 */
function param($name='',$default=''){
    if($name){
        return isset($_REQUEST[$name])?$_REQUEST[$name]:$default;
    }else{
        unset($_REQUEST[$this->request_url]);//卸载地址栏后面得url
        return $_REQUEST;
    }
}

/**
 * 获取请求参数 $_GET
 * @param string $name 参数键
 * @param string $default 不存在默认值
 * @return array|mixed|string 返回
 */
function get($name='',$default=''){
    if($name){
        return isset($_GET[$name])?$_GET[$name]:$default;
    }else{
        unset($_GET[$this->request_url]);//卸载地址栏后面得url
        return $_GET;
    }
}

/**
 * 获取请求参数 $_POST
 * @param string $name 参数键
 * @param string $default 不存在默认值
 * @return array|mixed|string 返回
 */
function post($name='',$default=''){
    if($name){
        return isset($_POST[$name])?$_POST[$name]:$default;
    }else{
        return $_POST;
    }
}

/**
 * 是否POST请求
 * @return bool
 */
function isPost(){
    return isset($_SERVER['REQUEST_METHOD']) && strtoupper($_SERVER['REQUEST_METHOD'])=='POST';
}

/**
 * 是否GET请求
 * @return bool
 */
function isGet(){
    return isset($_SERVER['REQUEST_METHOD']) && strtoupper($_SERVER['REQUEST_METHOD'])=='GET';
}

/**
 * 是否AJAX请求
 * @return bool
 */
function isAjax(){
    return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtoupper($_SERVER['HTTP_X_REQUESTED_WITH'])=='XMLHTTPREQUEST';
}

/**
 * 输出成功信息
 * @param $msg //信息内容
 * @param $data //返回数据
 * @return false|string
 */
function json_success($msg='成功',$data=''){
    echo json_encode(['code'=>1,'msg'=>$msg,'data'=>$data]);
    exit();
}

/**
 * 输出错误信息
 * @param string $msg
 * @param string $data
 * @return false|string
 */
function json_error($msg='失败',$data=''){
    echo json_encode(['code'=>0,'msg'=>$msg,'data'=>$data]);
    exit();
}

/**
 * 响应输出到浏览器，终止程序
 * @param string $msg
 * @param string $data
 */
function response_success($msg='成功',$data=''){
    echo json_encode(['code'=>1,'msg'=>$msg,'data'=>$data]);
    exit();
}

/**
 * 响应输出到浏览器，终止程序
 * @param string $msg
 * @param string $data
 */
function response_error($msg='失败',$data=''){
    echo json_encode(['code'=>0,'msg'=>$msg,'data'=>$data]);
    exit();
}

/**
 * 返回数据表格数据
 * @param string $code
 * @param string $msg
 * @param int $count
 * @param array $data
 */
function response_data($data=[],$count=0,$msg='OK'){
    echo json_encode(['code'=>0,'msg'=>$msg,'count'=>$count,'data'=>$data]);
    exit();
}

/**
 * 检查访问权限
 * @return bool
 */
function checkAuth($role=0){
    if($user = \core\Session::get('user')){
        if($role==0){
            return true;
        }

        $roles = array();
        if(is_array($role)){
            $roles = $role;
        }else{
            $roles[] = $role;
        }

        if(in_array($user['role'],$roles)){
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }
}