<?php
//常量定义
defined('DS') or define('DS', DIRECTORY_SEPARATOR);
defined('ROOT_PATH') or define('ROOT_PATH', realpath(dirname(dirname(__FILE__))).DS);//根目录

//引入自定义公共函数
require_once 'function.php';

// 注册系统自动加载
spl_autoload_register('autoload', true, true);

//自动加载函数
function autoload($class_name){

    $class_name = str_replace("\\",DS,$class_name);

    $FilePath = ROOT_PATH.$class_name.'.php';

    if(file_exists($FilePath)){
        require_once $FilePath;
    }else{
        \core\App::dump("自动加载不出类【".$class_name."】,路径:".$FilePath."有问题");
    }
}

//设置默认时区
date_default_timezone_set(\core\Config::get("app.default_timezone"));
error_reporting(\core\Config::get("app.error_reporting"));



//注册异常
register_shutdown_function('appShutdown');

//异常处理函数
function appShutdown(){
    $Error = error_get_last();
    if(!is_null($Error)){
        \core\App::dump($Error);
    }
}

session_start();
