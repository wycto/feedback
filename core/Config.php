<?php
namespace core;

class Config
{
    /**
     * @var array 配置参数
     */
    private static $config = null;

    /**
     * 获取配置
     * @param null $name
     * @param string $range
     * @return mixed|null
     */
    public static function get($name = null, $default = ''){
        if (empty(self::$config)){
            //把config目录下的配置文件全部加载
            $directory = ROOT_PATH."config";
            if(is_dir($directory)){
                $config_arr = scandir($directory);
                foreach ($config_arr as $config){
                    if(is_file($directory.DS.$config)){
                        $key = substr($config,0,-4);
                        $vueArr = require_once($directory.DS.$config);
                        self::$config[$key] = $vueArr;
                    }
                }
            }
        }

        if(empty($name)){
            return self::$config;
        }


        $keys = explode('.', $name, 2);
        if(count($keys)==2){
            return isset(self::$config[$keys[0]][$keys[1]])?self::$config[$keys[0]][$keys[1]]:$default;
        }else{
            return isset(self::$config[$keys[0]])?self::$config[$keys[0]]:$default;
        }
    }
}
