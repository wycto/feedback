<?php
namespace core;

class Controller
{
    protected $request;

    protected $template_data = [];

    function __construct()
    {
        $this->request = Request::instance();
    }

    /**
     * 给模板变量赋值
     * @param $name
     * @param string $value
     * @return $this
     */
    protected function assign($name, $value = '')
    {
        if (is_array($name)) {
            $this->template_data = array_merge($this->template_data, $name);
        } else {
            $this->template_data[$name] = $value;
        }
        return $this;
    }

    /**
     * 渲染模板文件
     * @param string $template 模板文件地址
     */
    protected function display($template = ''){
        $this->fetch($template);
    }

    /**
     * 使用指定模板
     * @param string $template
     */
    private function fetch($template = '')
    {
        if(!$template){
            $request = Request::instance();
            $template = ROOT_PATH;
            if($request->getModuleName()){
                $template .= $request->getModuleName() . DS;
            }
            $template .= 'view' . DS . $request->getControllerName() . DS . $request->getActionName() . '.php';
        }

        if(file_exists($template)){
            extract($this->template_data);
            include_once $template;
        }else{
            dump('模板文件' . $template . '不存在');
        }
    }
}