<?php
namespace core;

/**
 *
 * @author WeiYi
 *        
 */
class Log
{

    /**
     */
    function __construct()
    {}

    /**
     */
    function __destruct()
    {}

    /**
     * 写日志
     * @param $content //日志内容
     * @param string $type //日志类型：error、info、sql
     * @param bool $log_trace
     */
    static function write($content,$type='error',$log_trace=true){
        $date =date(Config::get('log.file_date'));
        $log_file_name = ROOT_PATH.'log'.DS.$type.$date.'.log';

        $wrap = "\r\n\r\n";
        if($log_trace){
            $wrap = "\r\n";
        }
        error_log('['.date('Y-m-d H:i:s').'] '.$content.$wrap,3,$log_file_name);

        if($log_trace) {
            //打印堆栈信息
            $traceArr = debug_backtrace();
            unset($traceArr[0]);
            $traceInfo = '';
            foreach($traceArr as $key=>$value) {
                $str = "";
                if(!empty($value['file'])){
                    $str .= "文件：".$value['file']."；";
                }

                if(!empty($value['line'])){
                    $str .= "行号：".$value['line']."；";
                }

                if(!empty($value['class'])){
                    $str .= "类：".$value['class']."；";
                }

                if(!empty($value['function'])){
                    $str .= "方法：".$value['function']."；";
                }

                if(!empty($value['type'])){
                    $str .= "类型：".$value['type']."；";
                }

                if(!empty($value['args'])){
                    $str .= "参数：";
                    foreach ($value['args'] as $arg){
                        if(is_array($arg)){
                            $str .= json_encode($arg,JSON_UNESCAPED_UNICODE)."；";
                        }else{
                            $str .= $arg."；";
                        }
                    }
                }

                $traceInfo .= "【".($key)."】".$str."\r\n";
            }
            error_log('['.date('Y-m-d H:i:s').'] '."堆栈信息：\r\n".$traceInfo."\r\n\r\n",3,$log_file_name);
        }
    }
}
