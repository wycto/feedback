<?php


namespace core;


class App
{
    static function dump($content){
        $debug = Config::get('app.debug');
        if($debug){
            dump($content);
        }else{
            $content = is_array($content)?json_encode($content,JSON_UNESCAPED_UNICODE):$content;
            Log::write($content,'error',false);
        }
    }
}