<?php
namespace core;

class Session
{

    function __construct()
    {

    }

    /**
     * 设置session
     * @param $key
     * @param $val
     */
    static function set($key,$val){
        $_SESSION[$key] = $val;
    }

    /**
     * 获取session
     * @param $key
     * @return mixed|string
     */
    static function get($key){
         return isset($_SESSION[$key])?$_SESSION[$key]:"";
    }

    static function delete($key){
        unset($_SESSION[$key]);
    }
}
