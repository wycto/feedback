/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50644
Source Host           : 127.0.0.1:3306
Source Database       : feedback

Target Server Type    : MYSQL
Target Server Version : 50644
File Encoding         : 65001

Date: 2021-02-23 23:46:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `category`
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT '分类名称',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '状态，1启用，0禁用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COMMENT='反馈分类表';

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1', '菜肴质量', '1');
INSERT INTO `category` VALUES ('9', '营养搭配', '1');
INSERT INTO `category` VALUES ('10', '收费标准', '1');
INSERT INTO `category` VALUES ('11', '结算方式', '1');
INSERT INTO `category` VALUES ('12', '设备管理', '1');
INSERT INTO `category` VALUES ('13', '安全管理', '1');
INSERT INTO `category` VALUES ('14', '卫生管理', '1');
INSERT INTO `category` VALUES ('15', '服务管理', '1');
INSERT INTO `category` VALUES ('16', '采购管理', '1');
INSERT INTO `category` VALUES ('17', '其他', '1');

-- ----------------------------
-- Table structure for `feedback`
-- ----------------------------
DROP TABLE IF EXISTS `feedback`;
CREATE TABLE `feedback` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT '0' COMMENT '反馈分类id',
  `category_name` varchar(64) DEFAULT '' COMMENT '反馈分类名称',
  `title` varchar(200) DEFAULT NULL COMMENT '反馈标题',
  `content` text COMMENT '反馈内容',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_id` int(11) DEFAULT NULL COMMENT '创建人',
  `create_name` varchar(32) DEFAULT NULL COMMENT '反馈人',
  `status` tinyint(2) DEFAULT '0' COMMENT '状态：0-待回复，1-已回复',
  `reply` varchar(20) DEFAULT NULL COMMENT '回复',
  `reply_time` datetime DEFAULT NULL COMMENT '回复时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='反馈信息表';

-- ----------------------------
-- Records of feedback
-- ----------------------------

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL DEFAULT '' COMMENT '登录账号',
  `password` char(32) NOT NULL COMMENT '登录密码',
  `role` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '角色',
  `create_time` datetime NOT NULL COMMENT '注册时间',
  `last_login_time` datetime NOT NULL COMMENT '最后登录时间',
  `last_login_ip` varchar(16) NOT NULL DEFAULT '' COMMENT '最后登录IP',
  `login_count` tinyint(11) unsigned NOT NULL DEFAULT '0' COMMENT '登录次数',
  `name` varchar(32) DEFAULT NULL COMMENT '姓名',
  `sex` varchar(3) DEFAULT NULL COMMENT '性别',
  `tel` varchar(32) DEFAULT NULL COMMENT '联系电话',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '账户状态：1启用，0禁用',
  PRIMARY KEY (`id`),
  KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='用户表';

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '1', '2021-01-30 22:43:56', '2021-02-23 23:23:26', '127.0.0.1', '1', null, null, null, '1');
INSERT INTO `user` VALUES ('2', 'weiyi', 'e10adc3949ba59abbe56e057f20f883e', '2', '2021-02-20 23:31:07', '2021-02-23 23:44:53', '127.0.0.1', '1', null, null, null, '1');
INSERT INTO `user` VALUES ('3', 'zhangsan', 'e10adc3949ba59abbe56e057f20f883e', '3', '2021-02-23 22:25:50', '2021-02-23 23:35:29', '127.0.0.1', '1', null, null, null, '1');
