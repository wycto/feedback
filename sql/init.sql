CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL DEFAULT '' COMMENT '登录账号',
  `password` char(32) NOT NULL COMMENT '登录密码',
  `role` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '角色',
  `create_time` datetime NOT NULL COMMENT '注册时间',
  `last_login_time` datetime NOT NULL COMMENT '最后登录时间',
  `last_login_ip` varchar(16) NOT NULL DEFAULT '' COMMENT '最后登录IP',
  `login_count` tinyint(11) unsigned NOT NULL DEFAULT '0' COMMENT '登录次数',
  `name` varchar(32) DEFAULT NULL COMMENT '姓名',
  `sex` varchar(3) DEFAULT NULL COMMENT '性别',
  `tel` varchar(32) DEFAULT NULL COMMENT '联系电话',
  PRIMARY KEY (`id`),
  KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='用户表';

CREATE TABLE `feedback` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT '0' COMMENT '反馈分类id',
  `category_name` varchar(64) DEFAULT '' COMMENT '反馈分类名称',
  `content` text COMMENT '反馈内容',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_uid` int(11) DEFAULT NULL COMMENT '创建人',
  `status` tinyint(2) DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='反馈信息表';

