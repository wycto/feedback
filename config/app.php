<?php
return [
    // 应用调试模式
    'debug' => false,
    //error_reporting错误级别
    'error_reporting' => 0,
    // 默认时区
    'default_timezone'       => 'PRC',
    // 默认模块名
    'default_module'         => 'admin',
    // 禁止访问模块
    'deny_module_list'       => ['common','config','core'],
    // 默认控制器名
    'default_controller'     => 'index',
    // 默认操作名
    'default_action'         => 'index',
    //过滤器,填写过滤函数名称，多个用英文逗号分隔
    'default_filter' => ''
];
