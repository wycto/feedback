<?php
require_once '../core/public.php';

if(isPost()){
    $username = post('username');
    if(empty($username)){
        response_error("请输入用户名");
    }

    $password = post('password');
    if(empty($password)){
        response_error("请填写密码");
    }

    $password2 = post('password2');
    if($password!=$password2){
        response_error("两次密码不相等");
    }

    //检查账号是否存在
    $user = \core\Db::connect()->table('user')->where(['username'=>$username])->find();
    if($user){
        response_error("账号已经存在，请更换账号注册");
    }

    //注册
    $data = array("username"=>$username,"password"=>md5($password));
    $data["create_time"] = date("Y-m-d H:i:s");
    $data["last_login_time"] = date("Y-m-d H:i:s");
    $data["role"] = 3;
    $bool = \core\Db::connect()->table("user")->insert($data);
    if($bool){
        response_success("注册成功");
    }else{
        response_error("注册失败");
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>登录界面</title>
    <link rel="stylesheet" href="/public/css/index.css">
    <script type="text/javascript" src="/public/js/jquery.js"></script>
</head>
<body>
<script>

    $(document).ready(function () {
        var whei = $(window).width()
        $("html").css({ fontSize: whei / 24 });
        $(window).resize(function () {
            var whei = $(window).width();
            $("html").css({ fontSize: whei / 24 })
        });
    });
</script>
<div class="main">

    <div class="header">
        <div class="header-center fl">
            <div class="header-title">
                注册界面
            </div>
            <div class="header-img"></div>
        </div>
        <div class="header-bottom fl"></div>

    </div>

    <div class="content">
        <div class="content-left">
            <!--<img src="images/d.png" alt="">-->
        </div>
        <form id="register-form">
            <div class="content-right">
                <div class="right-infp">
                    <div class="right-infp-name">
                        <input type="text" name="username" placeholder="请输入用户名" maxlength="12" required="" value="" autocomplete="off">
                    </div>
                    <div class="right-infp-name">
                        <input type="password" name="password" placeholder="请输入用密码" autocomplete="off">
                    </div>
                    <div class="right-infp-name">
                        <input type="password" name="password2" placeholder="请确认用密码" autocomplete="off">
                    </div>

                    <div class="right-infp-btn">
                        <button class="btn" id="register-btn">注  册</button>
                    </div>

                    <div style="text-align: right">
                        <a href="login.php" style="color: white">已有账号，去登录？</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="/public/plugins/layui/layui.all.js"></script>
<script>
    //js，layui的页面加载完成后的调用
    ;!function(){
        //jquery的post请求
        $("#register-btn").click(function () {
            $.post("",$("#register-form").serializeArray(),function (res) {
                if(res.code==1){
                    layer.alert(res.msg, {icon: 1},function () {
                        location.href="/admin/login.php";
                    });
                }else{
                    layer.alert(res.msg, {icon: 2});
                }
            },'json');
            return false;
        });
    }();
</script>
</body>
</html>
