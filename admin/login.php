<?php
require_once '../core/public.php';

if(isPost()){
    $username = post('username');
    if(empty($username)){
        response_error("请输入用户名");
    }

    $password = post('password');
    if(empty($password)){
        response_error("请填写密码");
    }

    $user = \core\Db::connect()->table('user')->where(['username'=>$username])->find();
    if($user){
        if($user["password"]!=md5($password)){
            response_error("密码不正确");
        }elseif($user["status"]==0){
            response_error();
        }else{
            \core\Session::set('user',$user);
            //登录信息修改
            $updateData = array(
                "last_login_time" => date("Y-m-d H:i:s"),
                "login_count" => $user["login_count"] + 1,
                "last_login_ip" => getIP()
            );
            \core\Db::connect()->table('user')->update($updateData,array("id"=>$user["id"]));
            response_success();
        }
    }else{
        response_error("用户不存在");
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>登录界面</title>
    <link rel="stylesheet" href="/public/css/index.css">
    <script type="text/javascript" src="/public/js/jquery.js"></script>
</head>
<body>
<script>

    $(document).ready(function () {
        var whei = $(window).width()
        $("html").css({ fontSize: whei / 24 });
        $(window).resize(function () {
            var whei = $(window).width();
            $("html").css({ fontSize: whei / 24 })
        });
    });
</script>
<div class="main">

    <div class="header">
        <div class="header-center fl">
            <div class="header-title">
                登录界面
            </div>
            <div class="header-img"></div>
        </div>
        <div class="header-bottom fl"></div>

    </div>

    <div class="content">
        <div class="content-left">
            <!--<img src="images/d.png" alt="">-->
        </div>
        <form id="login-form" class="content-right">
            <div class="right-infp">
                <div class="right-infp-name">
                    <input type="text" name="username" placeholder="请输入用户名" maxlength="12" required="" value="admin" autocomplete="off">
                </div>
                <div class="right-infp-name">
                    <input type="password" name="password" placeholder="请输入用密码" value="123456" autocomplete="off">
                </div>

                <div class="right-infp-btn">
                    <button id="login-btn" class="btn">登  录</button>
                </div>

                <div style="text-align: right">
                    <a href="register.php" style="color: white">没有账号，去注册？</a>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="/public/plugins/layui/layui.all.js"></script>
<script>
    //js，layui的页面加载完成后的调用
    ;!function(){
        //jquery的post请求
        $("#login-btn").click(function () {
            $.post("/admin/login.php",$("#login-form").serializeArray(),function (res) {
                if(res.code==1){
                    location.href="/admin/main.php";
                }else{
                    layer.alert(res.msg, {icon: 2});
                }
            },'json');
            return false;
        });
    }();
</script>
</body>
</html>