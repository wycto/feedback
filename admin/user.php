<?php
require_once("../core/public.php");
use \core\Db;
//权限检测
if(isGet() && checkAuth(1)==false){
    require_once '../public/403.html';
    exit();
}elseif (isPost()){
    if(checkAuth(1)==false){
        response_error();
    }else{
        $action = param('action');
        switch ($action){
            case "list":
                //数据查询
                $page = post('page');
                $userRows = Db::connect()->table('user')->page($page,15)->select();
                response_data($userRows,Db::connect()->table('user')->count());
                break;
            case "setField":
                $field = post('field');
                $value = post('value');
                $ids = post('ids');

                if($field=="role" && $value==1){
                    $ids = array_diff($ids,[1]);
                    if(count($ids)==0){
                        json_error("超级管理员不能设置为普通用户");
                    }
                }

                $ids_string  = implode(',',$ids);
                $result = Db::connect()->table('user')->update([$field=>$value],["id in (".$ids_string.")"]);
                if($result){
                    json_success("设置成功");
                }else{
                    json_error("设置失败");
                }
                break;
            default:
        }
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>用户管理</title>
    <link rel="stylesheet" href="/public/plugins/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/public/css/common.css" media="all">
    <script src="/public/plugins/layui/layui.js"></script>
    <script src="/public/js/public.js"></script>
</head>
<body>
<blockquote class="layui-elem-quote layui-quote-nm">用户管理</blockquote>
<div class="table-cc">
    <table class="layui-table" lay-filter="user-table" id="user-table"></table>
</div>
<script type="text/html" id="user-toolbar">
    <div class="layui-btn-container wycto-admin-toolbar-container">
        <button class="layui-btn layui-btn-sm layui-btn-normal" lay-event="setField" data-field="role" data-value="2">
            <i class="layui-icon layui-icon-set"></i>
            <span class="wycto-btn-text">设置为管理员</span>
        </button>
        <button class="layui-btn layui-btn-sm layui-btn-normal" lay-event="setField" data-field="role" data-value="3">
            <i class="layui-icon layui-icon-set"></i>
            <span class="wycto-btn-text">设置为普通用户</span>
        </button>
        <button class="layui-btn layui-btn-sm" lay-event="setField" data-field="status" data-value="1">
            <i class="layui-icon layui-icon-set"></i>
            <span class="wycto-btn-text">启用</span>
        </button>
        <button class="layui-btn layui-btn-sm layui-btn-danger" lay-event="setField" data-field="status" data-value="0">
            <i class="layui-icon layui-icon-set"></i>
            <span class="wycto-btn-text">禁用</span>
        </button>
    </div>
</script>
<script>
    layui.use('table', function(){
        var table = layui.table,$ = layui.$;

        //第一个实例
        tableIns = table.render({
            elem: '#user-table'
            ,title:"用户管理"
            ,url: '/admin/user.php' //数据接口
            ,where:{"action":"list"}
            ,page: true //开启分页
            ,method: 'POST'
            ,cellMinWidth: 10
            ,even: true //开启隔行背景
            ,autoSort: false //禁用前端自动排序。注意：该参数为 layui 2.4.4 新增
            ,toolbar: '#user-toolbar' //开启头部工具栏，并为其绑定左侧模板
            ,cols: [[ //表头
                {type: 'checkbox', fixed: 'left',field:'id'}
                ,{type: 'numbers', fixed: 'left',title:'序号'}
                ,{field: 'username', title: '用户名'}
                ,{field: 'name', title: '姓名'}
                ,{field: 'sex', title: '性别', sort: true}
                ,{field: 'tel', title: '联系电话', sort: true}
                ,{field: 'role', title: '角色',templet:function (d) {return d.role==1?"超级管理员":(d.role==2?"管理员":"普通用户");}}
                ,{field: 'create_time', title: '注册时间', sort: true}
                ,{field: 'last_login_time', title: '最后登录时间', sort: true}
                ,{field: 'last_login_ip', title: '最后登录IP'}
                ,{field: 'login_count', title: '登录次数', sort: true}
                ,{field: 'status', title: '登录次数', sort: true,templet:function (d) {return formatStatus(d.status);}}
            ]]
        });

        //头工具栏事件
        table.on('toolbar(user-table)', function(obj){
            var dataset = obj.config.elem.context.activeElement.dataset;
            var checkStatus = table.checkStatus(obj.config.id);
            switch(obj.event){
                case 'setField':
                    var data = checkStatus.data;
                    if(data.length<=0){
                        layer.alert('请勾选要设置的记录', {icon: 5});
                        return false;
                    }
                    var ids = [];
                    $(data).each(function(i) {
                        ids[i] = data[i]['id'];
                    });

                    $.post("", {ids:ids,action:'setField',field:dataset.field,value:dataset.value}, function(res) {
                        if (res.code) {
                            //询问框
                            tableIns.reload();
                            layer.msg(res.msg,{time:2000});
                        } else {
                            //信息框
                            layer.open({
                                content : res.msg,
                                btn : '我知道了'
                            });
                        }
                    },'json');
                    break;
            };
        });
    });
</script>
</body>
</html>
