<?php
require_once("../core/public.php");
if(checkAuth()==false){
    require_once '../public/403.html';
    exit();
}

//查询数量
$user = \core\Session::get("user");
if($user["role"]==1 || $user["role"]==2){
    //超级管理员、管理员
    $countTotal = \core\Db::connect()->table('feedback')->count();
    $countNoCheck = \core\Db::connect()->table('feedback')->where(['status'=>0])->count();
    $countChecked = \core\Db::connect()->table('feedback')->where(['status'=>1])->count();
}else{
    //普通用户
    $countTotal = \core\Db::connect()->table('feedback')->where(['create_id'=>$user["id"]])->count();
    $countNoCheck = \core\Db::connect()->table('feedback')->where(['create_id'=>$user["id"],'status'=>0])->count();
    $countChecked = \core\Db::connect()->table('feedback')->where(['create_id'=>$user["id"],'status'=>1])->count();
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>校园食堂餐饮服务反馈系统</title>
    <link rel="stylesheet" href="/public/plugins/layui/css/layui.css">
    <style>
        .item-row{text-align: center;margin-top: 10px;}
        .item-row a{
            background-color: #F2F2F2;
            display: inline-block;
            width: 95%;
            line-height: 20px;
            text-align: center;
            font-size: 18px;
            padding: 10px;
        }
    </style>
</head>
<body>
<blockquote class="layui-elem-quote layui-quote-nm">反馈信息</blockquote>
<div class="layui-row item-row">
    <div class="layui-col-md4">
        <a href="">总计数量</a>
    </div>
    <div class="layui-col-md4">
        <a href="">待回复</a>
    </div>
    <div class="layui-col-md4">
        <a href="">已回复</a>
    </div>
    <div class="layui-col-md4">
        <a href=""><?php echo $countTotal;?></a>
    </div>
    <div class="layui-col-md4">
        <a href=""><?php echo $countNoCheck;?></a>
    </div>
    <div class="layui-col-md4">
        <a href=""><?php echo $countChecked;?></a>
    </div>
</div>
</body>
</html>