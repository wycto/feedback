<?php
require_once("../core/public.php");
$user = \core\Session::get('user');
if(!$user){
    header('Location:/admin/login.php');
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>校园食堂餐饮服务反馈系统</title>
    <link rel="stylesheet" href="/public/plugins/layui/css/layui.css">
    <style>
        #iframe{
            border: none;
            position: absolute;
            width: 100%;
            height: 100%;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0;
        }
    </style>
</head>
<body class="layui-layout-body" id="main-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header" id="main-header">
        <div class="layui-logo">校园食堂餐饮服务反馈系统</div>
        <!-- 头部区域（可配合layui已有的水平导航） -->
        <!--<ul class="layui-nav layui-layout-left">
            <li class="layui-nav-item"><a href="">控制台</a></li>
            <li class="layui-nav-item"><a href="">商品管理</a></li>
            <li class="layui-nav-item"><a href="">用户</a></li>
            <li class="layui-nav-item">
                <a href="javascript:;">其它系统</a>
                <dl class="layui-nav-child">
                    <dd><a href="">邮件管理</a></dd>
                    <dd><a href="">消息管理</a></dd>
                    <dd><a href="">授权管理</a></dd>
                </dl>
            </li>
        </ul>-->
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item">
                <a href="javascript:;">
                    <!--<img src="http://t.cn/RCzsdCq" class="layui-nav-img">-->
                    <?php echo $user["name"]?$user["name"]:$user["username"]?>
                </a>
                <dl class="layui-nav-child">
                    <dd><a href="/admin/userinfo.php" target="iframe">基本资料</a></dd>
                    <dd><a href="/admin/pwd.php" target="iframe">密码修改</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item"><a href="/admin/loginout.php">退出</a></li>
        </ul>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree"  lay-filter="test">
                <li class="layui-nav-item layui-nav-itemed"><a href="/admin/index.php" target="iframe">首页</a></li>
                <?php if($user['role']==1):?>
                <li class="layui-nav-item layui-nav-itemed"><a href="/admin/user.php" target="iframe">用户管理</a></li>
                <li class="layui-nav-item layui-nav-itemed"><a href="/admin/category.php" target="iframe">反馈分类</a></li>
                <?php endif;?>
                <?php if ($user['role']==1 || $user['role']==2):?>
                <li class="layui-nav-item layui-nav-itemed"><a href="/admin/record.php" target="iframe">反馈信息</a></li>
                <?php endif;?>
                <?php if ($user['role']==3):?>
                <li class="layui-nav-item layui-nav-itemed"><a href="/admin/my.php" target="iframe">我的反馈</a></li>
                <?php endif;?>
                <!--<li class="layui-nav-item">
                    <a href="javascript:;">我的反馈</a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:;">信息反馈</a></dd>
                        <dd><a href="javascript:;">反馈审核</a></dd>
                        <dd><a href="">超链接</a></dd>
                    </dl>
                </li>-->
            </ul>
        </div>
    </div>

    <div class="layui-body" id="lay-content">
        <!-- 内容主体区域 -->
        <div style="padding: 15px;">
            <iframe id="iframe" name="iframe" src="/admin/index.php"></iframe>
        </div>
    </div>

    <div class="layui-footer" id="main-footer">
        <!-- 底部固定区域 -->
        ©  校园食堂餐饮服务反馈系统
    </div>
</div>
<script src="/public/plugins/layui/layui.js"></script>
<script>
    //JavaScript代码区域
    layui.use('element', function(){
        var element = layui.element;
        var $ = layui.$;
        /*var height = $("#lay-content").height();
        var main_header = $("#main-header").height();
        var main_footer = $("#main-footer").height();
        $("#iframe").css({ max-height:height });
        $(window).resize(function () {
            var whei = $(window).width();
            $("html").css({ fontSize: whei / 24 })
        });*/
    });
</script>
</body>
</html>
