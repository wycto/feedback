<?php
require_once("../core/public.php");
use \core\Db;
//权限检测
if(isGet() && checkAuth(["1","2"])==false){
    require_once '../public/403.html';
    exit();
}elseif (isPost()){
    if(checkAuth(["1","2"])==false){
        response_error();
    }else{
        $user = \core\Session::get("user");
        $action = param('action');
        switch ($action){
            case "detail":
                $id = post('id');
                $row = Db::connect()->table('category')->where(['id'=>$id])->find();
                if($row){
                    json_success("Ok",$row);
                }else{
                    json_error("数据不存在");
                }
                break;
            case "list":
                //数据查询
                $page = post('page');
                $userRows = Db::connect()->table('feedback')->page($page,15)->select();
                $count = Db::connect()->table('feedback')->count();
                response_data($userRows,$count);
                break;
            case "add":
                $data = post('data');
                $data['create_time'] = date("Y-m-d H:i:s");
                $data['create_id'] = $user['id'];
                $data['create_name'] = $user['name']?$user['name']:$user['username'];
                $category = Db::connect()->table('category')->where(['id'=>$data['category_id']])->find();
                $data['category_name'] = $category['name'];

                $result = Db::connect()->table('feedback')->insert($data);
                if($result){
                    json_success("添加成功");
                }else{
                    json_error("添加失败");
                }
                break;
            case "edit":
                $data = post('data');
                $id = post('id');

                unset($data['id']);

                $data['reply_time'] = date("Y-m-d H:i:s");
                $data['status'] = 1;
                $result = Db::connect()->table('feedback')->update($data,["id"=>$id]);
                if($result){
                    json_success("修改成功");
                }else{
                    json_error("修改失败");
                }
                break;
            default:

        }
    }
}else{
    $categorys = Db::connect()->table('category')->select();
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>我的反馈</title>
    <link rel="stylesheet" href="/public/plugins/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/public/css/common.css" media="all">
    <script src="/public/plugins/layui/layui.js"></script>
    <script src="/public/js/public.js"></script>
</head>
<body>
<blockquote class="layui-elem-quote layui-quote-nm">我的反馈</blockquote>
<div class="table-cc">
    <table class="layui-table" lay-filter="user-table" id="user-table"></table>
</div>

<form class="layui-form layui-hide" action="" id="formAdd" style="padding: 20px" lay-filter="formAddEdit">
    <input type="hidden" name="id">
    <div class="layui-form-item">
        <label class="layui-form-label">反馈分类</label>
        <div class="layui-input-block">
            <select name="category_id" lay-verify="required" disabled>
                <option value="">-请选择-</option>
                <?php foreach ($categorys as $category):?>
                    <option value="<?php echo $category['id']?>"><?php echo $category['name']?></option>
                <?php endforeach;?>
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">反馈标题</label>
        <div class="layui-input-block">
            <input type="text" name="title" disabled required  lay-verify="required" placeholder="请输入反馈标题" autocomplete="off" class="layui-input layui-disabled">
        </div>
    </div>
    <div class="layui-form-item layui-form-text">
        <label class="layui-form-label">反馈内容</label>
        <div class="layui-input-block">
            <textarea name="content" disabled readonly placeholder="请输入反馈内容" lay-verify="required" class="layui-textarea layui-disabled" rows="10"></textarea>
        </div>
    </div>
    <div class="layui-form-item layui-form-text">
        <label class="layui-form-label">回复内容</label>
        <div class="layui-input-block">
            <textarea name="reply" placeholder="请输入反馈内容" lay-verify="required" class="layui-textarea" rows="6"></textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formAddEdit">提交</button>
        </div>
    </div>
</form>

<div class="layui-form layui-hide" id="formDetail" style="padding: 20px" lay-filter="formDetail">
    <input type="hidden" name="id">
    <div class="layui-form-item">
        <label class="layui-form-label">反馈分类</label>
        <div class="layui-input-block">
            <input type="text" name="category_name" disabled required  lay-verify="required" placeholder="请输入反馈标题" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">反馈标题</label>
        <div class="layui-input-block">
            <input type="text" name="title" disabled required  lay-verify="required" placeholder="请输入反馈标题" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item layui-form-text">
        <label class="layui-form-label">反馈内容</label>
        <div class="layui-input-block">
            <textarea name="content" disabled readonly placeholder="请输入反馈内容" lay-verify="required" class="layui-textarea" rows="10"></textarea>
        </div>
    </div>
    <div class="layui-form-item layui-form-text">
        <label class="layui-form-label">回复内容</label>
        <div class="layui-input-block">
            <textarea name="reply" disabled readonly placeholder="请输入反馈内容" lay-verify="required" class="layui-textarea" rows="6"></textarea>
        </div>
    </div>
</div>

<script type="text/html" id="barDemo">
    <a class="layui-btn layui-btn-xs  layui-btn-normal" lay-event="detail">详情</a>
    <?php $user = \core\Session::get("user");if($user['role']==1 || $user['role']==2):?>
    {{#  if(d.status == 0){ }}
    <a class="layui-btn layui-btn-xs" lay-event="edit">回复</a>
    {{#  } }}
    <?php endif;?>
</script>

<script>
    layui.use(['table','form'], function(){
        var table = layui.table,$ = layui.$,form = layui.form;

        //第一个实例
        tableIns = table.render({
            elem: '#user-table'
            ,title:"用户管理"
            ,url: '/admin/record.php?action=list' //数据接口
            ,page: true //开启分页
            ,method: 'POST'
            ,cellMinWidth: 10
            ,even: true //开启隔行背景
            ,autoSort: false //禁用前端自动排序。注意：该参数为 layui 2.4.4 新增
            ,cols: [[ //表头
                {type: 'checkbox', fixed: 'left',field:'id'}
                ,{type: 'numbers', fixed: 'left',title:'序号'}
                ,{field: 'category_name', title: '反馈分类'}
                ,{field: 'title', title: '反馈标题'}
                ,{field: 'content', title: '反馈内容'}
                ,{field: 'create_time', title: '反馈时间'}
                ,{field: 'create_name', title: '反馈人'}
                ,{field: 'reply', title: '回复内容'}
                ,{field: 'reply_time', title: '回复时间'}
                ,{field: 'op', title: '操作',toolbar:'#barDemo'}
            ]]
        });

        //监听工具条
        table.on('tool(user-table)', function(obj){
            var data = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）

            if(layEvent === 'edit'){ //编辑
                //do something
                //给表单赋值
                form.val("formAddEdit", data);
                $("#formAdd").removeClass('layui-hide');
                layer.open({
                    type: 1,
                    skin: 'layui-layer-rim', //加上边框
                    area: ['800px', '650px'], //宽高
                    content: $('#formAdd'),
                    cancel: function(index, layero){
                        $("#formAdd").addClass('layui-hide');
                        layer.close(index);
                        return false;
                    }
                });
            }else if(layEvent === 'detail'){
                form.val("formDetail", data);
                $("#formDetail").removeClass('layui-hide');
                layer.open({
                    type: 1,
                    skin: 'layui-layer-rim', //加上边框
                    area: ['800px', '650px'], //宽高
                    content: $('#formDetail'),
                    cancel: function(index, layero){
                        $("#formDetail").addClass('layui-hide');
                        layer.close(index);
                        return false;
                    }
                });
            }
        });

        //监听提交
        form.on('submit(formAddEdit)', function(data){
            var action = "add";
            var dataRow = data.field;
            if(dataRow.id){
                action = "edit";
            }else{
                delete dataRow.id
            }
            //layer.msg(JSON.stringify(data.field));
            $.post("", {action:action,data:data.field,id:dataRow.id}, function(res) {
                if (res.code) {
                    //询问框
                    tableIns.reload();
                    layer.msg(res.msg,{time:2000});
                    $("#formAdd").addClass('layui-hide');
                    layer.closeAll(); //疯狂模式，关闭所有层
                } else {
                    //信息框
                    layer.open({
                        content : res.msg,
                        btn : '我知道了'
                    });
                }
            },'json');
            return false;
        });
    });
</script>
</body>
</html>