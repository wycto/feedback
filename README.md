# PHP项目

#### 介绍

admin:后台管理

config：配置文件

core：核心文件，不要改动

log：日志文件夹

model：模型类库目录

public： 静态资源文件夹，存放js、css、img

sql：sql语句

.htaccess：重写文件

入口文件：index.php


# 部署

配置域名访问到 : index.php

比如配置域名 www.fb.me 目录指向 index.php


浏览器访问： www.fb.me

 www.fb.me/module/controller/action
 
 module:模块，对应一个目录，比如admin
 
 controller：控制器，对应  模块目录下的PHP文件
 
 action：动作方法，php文件里面的 switch


比如：

http://www.fb.me/admin/user/login

对应 admin\login.php

login.php 里面有一个switch,  case 有一个login
