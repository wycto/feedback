function formatStatus(status,color=true){
    var name = "";
    var color = "";
    if(status==1){
        name = "启用";
        color = "green";
    }else if(status==0){
        name = "禁用";
        color = "red";
    }

    return '<span style="color: ' + color + '">' + name + '</span>'
}